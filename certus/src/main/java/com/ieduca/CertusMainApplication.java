package com.ieduca;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CertusMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CertusMainApplication.class, args);
    }
}