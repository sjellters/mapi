package com.ieduca.util;

public class AuthorizationHandler {

    public static boolean isAuthorized(int value) {
        return value % 2 == 0;
    }
}
