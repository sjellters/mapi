package com.ieduca;

import org.springframework.boot.SpringApplication;

public class CommonMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(CommonMainApplication.class, args);
    }
}